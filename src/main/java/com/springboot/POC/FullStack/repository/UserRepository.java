package com.springboot.POC.FullStack.repository;

import org.springframework.data.repository.CrudRepository;

import com.springboot.POC.FullStack.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

}
