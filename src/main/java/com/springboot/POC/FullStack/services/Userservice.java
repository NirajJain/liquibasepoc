package com.springboot.POC.FullStack.services;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.springboot.POC.FullStack.model.User;
import com.springboot.POC.FullStack.repository.UserRepository;

@Service
@Transactional
public class Userservice {

	
	private final UserRepository userRepository;
	
	Userservice(UserRepository userRepository){
		this.userRepository=userRepository;
	}
	public void saveMyUser(User user) {
		userRepository.save(user);
	}
}
