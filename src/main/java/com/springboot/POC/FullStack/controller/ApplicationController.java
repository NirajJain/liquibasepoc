package com.springboot.POC.FullStack.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ApplicationController {

	@RequestMapping("/welcome")
	@ResponseBody
	public String sayhello() {
		return "Hello Commander..!! Command at your feet";
	}
	
	@RequestMapping("/")
	public String saypage(HttpServletRequest request) {
	request.setAttribute("mode","MODE_HOME");
		return "home";
	}
	
	@RequestMapping("/register")
	public String registration(HttpServletRequest request) {
       request.setAttribute("mode", "MODE_REGISTER");
		return "home";
	}
}
