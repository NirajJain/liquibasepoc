package com.springboot.POC.FullStack.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.POC.FullStack.model.User;
import com.springboot.POC.FullStack.services.Userservice;

@RestController
public class APIController {
 
	@Autowired
	private Userservice userservice;
	@GetMapping("/hello")
  public String hello() {
	return "This is home page";  
  }
		
	@GetMapping("/save-user")
	public String saveUser(@RequestParam String username,@RequestParam String firstname,@RequestParam String lastname,@RequestParam int age,@RequestParam String password) {
		User user=new User(username,firstname,lastname,age,password);
		userservice.saveMyUser(user);
		return "User is saved";
	}
}
